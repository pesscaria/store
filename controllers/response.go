package controllers

import (
	"errors"
)

// 通用错误❌
var (
	errParam   = errors.New("请求参数错误")
	errServer  = errors.New("内部服务错误")
	errOther   = errors.New("其他错误")
	errExpired = errors.New("token已过期")
)

// 同用错误描述
const (
	descParam   = "请求参数有误，请查看文档"
	descServer  = "服务器繁忙，请稍后再试"
	descExpired = "请重新登录"
)

// 通用返回码
const (
	codeOK         = 0
	codeErrParam   = 100
	codeErrServer  = 101
	codeErrOther   = 102
	codeErrExpired = 103
)

type resp struct {
	Code int         `json:"code"`
	Data interface{} `json:"data,omitempty"`
	Err  string      `json:"error,omitempty"`
	Desc string      `json:"description,omitempty"`
}

func failJSON(code int, err error, desc string) *resp {
	return &resp{
		Code: code,
		Err:  err.Error(),
		Desc: desc,
	}
}

func okJSON(data interface{}) *resp {
	return &resp{
		Code: 0,
		Data: data,
	}
}
