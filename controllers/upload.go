package controllers

import (
	"store/models"

	"bytes"
	"crypto/md5"
	"fmt"
	"image"
	"image/jpeg"

	"github.com/nfnt/resize"

	"encoding/json"

	"qiniupkg.com/api.v7/kodo"
)

//头像尺寸限制
const (
	AvatarMaxWidth  = 500
	AvatarMinHeight = 500
)

//UploadController ...
type UploadController struct {
	BaseController
}

//MakeUpToken 获取上传token
func (c *UploadController) MakeUpToken() {
	body := c.Ctx.Input.RequestBody
	var policy kodo.PutPolicy
	if err := json.Unmarshal(body, &policy); err != nil {
		models.LOGGER.Error("param invilid, reason: %v", err)
		c.FailJSON(codeErrParam, errParam, descParam)
		return
	}
	if policy.Scope == "" {
		models.LOGGER.Error("param invilid, reason: %s", "scope is null")
		c.FailJSON(codeErrParam, errParam, "scope is null")
		return
	}
	token := models.MakeUpToken(&policy)
	c.OKJSON(map[string]string{"token": token})
}

// GetToken 服务端设置上传策略
func (c *UploadController) GetToken() {
	bucketType := c.GetString("bucket", "")
	var bucket string
	switch bucketType {
	case "avatar":
		bucket = models.AvatarBucket
	case "photo":
		bucket = models.PhotoBucket
	case "video":
		bucket = models.VideoBucket
	default:
		models.LOGGER.Error("bucket error")
		c.FailJSON(codeErrParam, errParam, "bucket param error")
		return
	}
	policy := &kodo.PutPolicy{
		Scope:   bucket,
		Expires: 3600,
	}
	token := models.MakeUpToken(policy)
	c.Ctx.Output.Header("Access-Control-Allow-Origin", "*")
	c.Data["json"] = map[string]string{"uptoken": token}
	c.ServeJSON()
}

//UpdateAvatar 更新头像
func (c *UploadController) UpdateAvatar() {
	_img := c.Ctx.Input.RequestBody
	//文件大小范围 1KB ~ 5MB
	if len(_img) < 1024 || len(_img) > 1024*1024*5 {
		models.LOGGER.Error("param invilid, reason: picture size invilid")
		c.FailJSON(codeErrParam, errParam, "picture size invilid")
		return
	}

	reader := bytes.NewReader(_img)
	rawImg, format, err := image.Decode(reader)
	models.LOGGER.Debug("image fmt: %s", format)
	if err != nil {
		models.LOGGER.Error("image.Decode error,reason: %v", err)
		c.FailJSON(codeErrParam, errParam, "picture fmt invilid")
		return
	}
	var (
		fileKey string
		img     = new(bytes.Buffer)
	)

	mp := rawImg.Bounds().Max
	if mp.X > AvatarMaxWidth || mp.Y > AvatarMinHeight {
		resizedImg := resize.Resize(AvatarMaxWidth, AvatarMinHeight, rawImg, resize.Lanczos3)
		jpeg.Encode(img, resizedImg, nil)
		fileKey = fmt.Sprintf("%s.jpg", fmt.Sprintf("%x", md5.Sum(img.Bytes())))
	} else {
		fileKey = fmt.Sprintf("%s.jpg", fmt.Sprintf("%x", md5.Sum(_img)))
		img.Write(_img)
	}

	imgURL, err := models.UploadDataWihtKey(img.Bytes(), fileKey)
	if err != nil {
		models.LOGGER.Error("UploadAvatar error, reason: %v", err)
		c.FailJSON(codeErrServer, errServer, err.Error())
		return
	}
	c.OKJSON(map[string]string{"avatar": imgURL})
}
