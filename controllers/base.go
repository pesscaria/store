package controllers

import (
	"github.com/astaxie/beego"
)

// BaseController ...
type BaseController struct {
	beego.Controller
}

func (c *BaseController) Prepare() {
	if c.Ctx.Request.Method == "OPTIONS" {
		c.Ctx.Output.Header("Access-Control-Allow-Origin", "*")
		c.Ctx.Output.Header("Access-Control-Allow-Headers", c.Ctx.Input.Header("Access-Control-Request-Headers"))
		c.Ctx.Output.Header("Access-Control-Allow-Method", c.Ctx.Input.Header("Access-Control-Request-Method"))
		c.StopRun()
		return
	}
}

// OKJSON 成功json回复
func (c *BaseController) OKJSON(data interface{}) {
	c.Data["json"] = okJSON(data)
	c.ServeJSON()

}

// FailJSON 失败json回复
func (c *BaseController) FailJSON(code int, err error, desc string) {
	c.Data["json"] = failJSON(code, err, desc)
	c.ServeJSON()
}
