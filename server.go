package main

import (
	"store/models"
	pb "store/proto"

	"net"
	"os"
	"strings"
	"time"

	"github.com/astaxie/beego"
	sd "github.com/bohler/lib/etcd-sd/grpc"
	"github.com/coreos/etcd/clientv3"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type server struct {
}

func (s *server) UploadWithKey(ctx context.Context, in *pb.Store_File) (*pb.Store_FilePath, error) {
	var out pb.Store_FilePath
	url, err := models.UploadDataWihtKey(in.Data, in.Key)
	if err != nil {
		models.LOGGER.Error("%v", err)
		return nil, err
	}
	out.Url = url
	return &out, nil
}

func (s *server) Run(closeSig chan bool) {
	lis, err := net.Listen("tcp", beego.AppConfig.String("rpc_listen"))
	if err != nil {
		models.LOGGER.Error("%v", err)
		os.Exit(-1)
	}
	rs := grpc.NewServer()
	// rpc服务注册
	pb.RegisterStoreServiceServer(rs, s)

	go rs.Serve(lis)
	ep := beego.AppConfig.String("etcd_endpoints")
	etcdCfg := clientv3.Config{
		Endpoints:   strings.Split(ep, ","),
		DialTimeout: time.Duration(beego.AppConfig.DefaultInt64("etcd_timeout", 3)) * time.Second,
	}
	models.LOGGER.Debug("%v", etcdCfg.Endpoints)

	// 服务发现注册
	sd.ServerPoolInit(etcdCfg)
	sd.CreateSelf(beego.AppConfig.String("rpc_selfname"), beego.AppConfig.String("rpc_path"))

	//sd.RegisterService(etcdCfg, config.AppConfig.RPCListen.Name, config.AppConfig.RPCListen.Listen)
	models.LOGGER.Info("listening on %v", lis.Addr())
	go beego.Run()

	<-closeSig
}

func (s *server) OnInit() {}

func (s *server) OnDestroy() {}
