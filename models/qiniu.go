package models

import (
	"github.com/astaxie/beego"
	"qiniupkg.com/api.v7/kodo"
	"qiniupkg.com/api.v7/kodocli"
)

//PutRet 上传返回字段
type PutRet struct {
	Hash string `json:"hash"`
	Key  string `json:"key"`
}

//Qiniu
var (
	AvatarBucket = beego.AppConfig.String("avatar_bucket")
	VideoBucket  = beego.AppConfig.String("video_bucket")
	PhotoBucket  = beego.AppConfig.String("photo_bucket")

	AvatarHost = beego.AppConfig.String("avatar_host")
	VideoHost  = beego.AppConfig.String("video_host")
	PhotoHost  = beego.AppConfig.String("photo_host")

	QNClient   *kodo.Client
	QNUploader kodocli.Uploader
)

// MakeUpToken 根据上传策略生成上传token
func MakeUpToken(policy *kodo.PutPolicy) string {
	LOGGER.Debug("%v", *policy)
	token := QNClient.MakeUptoken(policy)
	return token
}

//UpFile 上传文件
func UpFile(policy *kodo.PutPolicy, file string) {
	token := MakeUpToken(policy)
	var ret PutRet
	if res := QNUploader.PutFileWithoutKey(nil, &ret, token, file, nil); res != nil {
		LOGGER.Error("%v", res)
	}
	LOGGER.Debug("%v", ret)
}

//DeleteFile 删除文件
func DeleteFile() {

}

func init() {
	zone := 0
	kodo.SetMac(beego.AppConfig.String("access_key"), beego.AppConfig.String("secret_key"))
	QNClient = kodo.New(zone, nil)
	QNUploader = kodocli.NewUploader(zone, nil)
	//LOGGER.Debug("qi niu init")
}
