package models

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
)

const (
	LOG_FLAG = log.Ldate | log.Ltime | log.Lmicroseconds
)

var DEBUG = beego.AppConfig.DefaultBool("debug", true)
var LOG_ADAPTER = beego.AppConfig.String("log_adapter")
var LOG_PARAMS = beego.AppConfig.String("log_params")

var WRITER = getWriter(DEBUG)
var LOGGER = logs.NewLogger(1000)

func getWriter(debug bool) io.Writer {
	if debug {
		return os.Stdout
	}

	now := time.Now()
	year, month, day := now.Date()
	hour, min, sec := now.Clock()

	// log目录下按照当前时间创建日志文件
	format := "log/%d-%d-%d-%d-%d-%d.log"
	name := fmt.Sprintf(format, year, month, day, hour, min, sec)

	f, err := os.Create(name)
	if err != nil {
		fmt.Errorf("create log file err: %v\n", err)
		return getWriter(true)
	}

	return f
}

func LogRequest(req *http.Request) {
	LOGGER.Debug("req: %s %s %s %s", req.RemoteAddr, req.Proto, req.Method, req.RequestURI)
	if DEBUG {
		for k, v := range req.Header {
			LOGGER.Debug("req header: %s = %s", k, v)
		}
	}
}

func LogResponse(resp *http.Response) {
	LOGGER.Debug("resp: %s %s %s", resp.Request.RequestURI, resp.Proto, resp.Status)
	if DEBUG {
		for k, v := range resp.Header {
			LOGGER.Debug("req header: %s = %s", k, v)
		}
	}
}

func Itoa(buf *[]byte, i int, wid int) {
	// Assemble decimal in reverse order.
	var b [20]byte
	bp := len(b) - 1
	for i >= 10 || wid > 1 {
		wid--
		q := i / 10
		b[bp] = byte('0' + i - q*10)
		bp--
		i = q
	}
	// i < 10
	b[bp] = byte('0' + i)
	*buf = append(*buf, b[bp:]...)
}

func GetString(m map[string]interface{}, k string) string {
	return ToString(m[k])
}

func GetInt(m map[string]interface{}, k string) int64 {
	return ToInt(m[k])
}

func GetFloat(m map[string]interface{}, k string) float64 {
	return ToFloat(m[k])
}

func ToString(v interface{}) string {
	switch v.(type) {
	case string:
		return v.(string)
	case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
		return fmt.Sprintf("%d", v)
	case float32, float64:
		return fmt.Sprintf("%f", v)
	default:
		return fmt.Sprintf("%v", v)
	}
}

func ToInt(v interface{}) int64 {
	switch v.(type) {
	case string:
		i, e := strconv.ParseInt(v.(string), 10, 64)
		if e != nil {
			return -1
		} else {
			return int64(i)
		}
	case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
		return v.(int64)
	case float32, float64:
		return int64(v.(float64))
	default:
		return -1
	}
}

func ToFloat(v interface{}) float64 {
	switch v.(type) {
	case string:
		f, e := strconv.ParseFloat(v.(string), 64)
		if e != nil {
			return -1
		} else {
			return float64(f)
		}
	case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
		return v.(float64)
	case float32, float64:
		return v.(float64)
	default:
		return -1
	}
}

func init() {
	LOGGER.Info("@@@@@@@@@ init log start @@@@@@@@")

	LOGGER.Info("log params: %s\n", LOG_PARAMS)
	LOGGER.Info("log adapter: %s\n", LOG_ADAPTER)

	if err := LOGGER.SetLogger(LOG_ADAPTER, LOG_PARAMS); err != nil {
		LOGGER.Error("create beego log err: %s", err.Error())
		panic(-1)
	}

	if err := LOGGER.SetLogger("console", `{"level":7}`); err != nil {
		LOGGER.Error("create beego log err: %s", err.Error())
		panic(-1)
	}

	LOGGER.EnableFuncCallDepth(true)
	LOGGER.SetLogFuncCallDepth(3)

	LOGGER.Info("@@@@@@@@@ init log end @@@@@@@@@")
}
