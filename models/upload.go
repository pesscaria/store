package models

import (
	"bytes"
	"fmt"

	"golang.org/x/net/context"
	"qiniupkg.com/api.v7/kodo"
	"qiniupkg.com/api.v7/kodocli"
)

// UploadDataWihtKey 上传文件
func UploadDataWihtKey(img []byte, key string) (string, error) {
	_img := bytes.NewReader(img)
	var putRet kodocli.PutRet
	upToken := MakeUpToken(&kodo.PutPolicy{
		Scope:   AvatarBucket + ":" + key,
		Expires: 3600,
	})
	LOGGER.Debug("uptoken:%s", upToken)
	if err := QNUploader.Put(context.Background(), &putRet, upToken, key, _img, int64(_img.Len()), nil); err != nil {
		LOGGER.Error("put file error, reason: %v", err)
		return "", err
	}
	LOGGER.Debug("%v", putRet)
	return fmt.Sprintf("%s/%s", AvatarHost, key), nil
}
