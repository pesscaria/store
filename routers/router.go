package routers

import (
	"store/controllers"

	"github.com/astaxie/beego"
)

func init() {

	storecenter := beego.NewNamespace("/storecenter",
		//beego.NSRouter("/", &controllers.MainController{}),
		beego.NSRouter("/upload/makeuptoken", &controllers.UploadController{}, "post:MakeUpToken"),
		beego.NSRouter("/upload/token", &controllers.UploadController{}, "get,options:GetToken"),
		beego.NSRouter("/upload/avatar", &controllers.UploadController{}, "post:UpdateAvatar"),
	)
	beego.AddNamespace(storecenter)
}
