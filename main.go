package main

import (
	_ "store/routers"

	"github.com/bohler/halo/module"
)

func main() {
	module.Run((&server{}))
}
