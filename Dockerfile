FROM alpine:latest

MAINTAINER 610960686@qq.com

COPY  storecenter /go/storecenter
COPY  conf/app.conf /go/conf/app.conf

RUN apk --no-cache add ca-certificates bash wget
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub \
RUN wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.23-r3/glibc-2.23-r3.apk \
RUN apk add glibc-2.23-r3.apk 

WORKDIR /go

RUN chmod +x /go/storecenter

EXPOSE 8090

ENTRYPOINT ["/go/storecenter"]

sudo docker run \
	--volume /var/lib/drone:/var/lib/drone \
	--volume /var/run/docker.sock:/var/run/docker.sock \
	--env-file /etc/drone/dronerc \
	--restart=always \
	--publish=80:8000 \
	--detach=true \
	--name=drone \
    --link=postgresql:postgres \
	drone/drone:0.4


